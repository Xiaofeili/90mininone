# base image
FROM gpdowning/python


# Install dependencies
RUN pip install Flask
RUN pip install Flask-SQLAlchemy
RUN pip install Flask-Restless
RUN pip install pymysql
RUN pip install -U flask-cors
RUN pip install requests
RUN pip install mysql-connector-python
RUN pip install python-dotenv
RUN pip3 install mysqlclient

EXPOSE 5000
