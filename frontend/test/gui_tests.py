from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from unittest import main, TestCase
import time


class GuiTestSelenium(TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        self.driver.get("https://90mininone.me/")

    def test_1(self):
        self.assertEqual(self.driver.title, "90Min In One")

    def test_2(self):
        self.driver.find_element_by_link_text("Leagues").click()
        self.assertIn("https://90mininone.me/" + "Leagues", self.driver.current_url)
    
    def test_3(self):
        self.driver.find_element_by_link_text("Teams").click()
        self.assertIn("https://90mininone.me/" + "Teams", self.driver.current_url)
    
    def test_4(self):
        self.driver.find_element_by_link_text("Players").click()
        self.assertIn("https://90mininone.me/" + "Players", self.driver.current_url)
    
    def test_5(self):
        self.driver.get("https://90mininone.me/Leagues")
        time.sleep(6)
        self.driver.find_element_by_link_text("More Info").click()
        self.assertIn("https://90mininone.me/Leagues/" + "Ykkonen", self.driver.current_url)
    
    def test_6(self):
        self.driver.get("https://90mininone.me/Teams")
        time.sleep(10)
        self.driver.find_element_by_link_text("More Info").click()
        self.assertIn("https://90mininone.me/Teams/" + "12de%20Octubre", self.driver.current_url)

    def test_7(self):
        self.driver.get("https://90mininone.me/Players")
        time.sleep(10)
        self.driver.find_element_by_link_text("More Info").click()
        self.assertIn("https://90mininone.me/Players/" + "A.Armstrong", self.driver.current_url)

if __name__ == "__main__":  # pragma: no cover
    main()
