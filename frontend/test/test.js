const assert = require('assert');
const pagination = '../Pagination.js';
const req = require('request');
const baseUrl = "http://localhost:3000/";

it("should not return null", function() {
  req(baseUrl, function(error, response, body) {
    assert.notEqual(baseUrl, null);
  });
});

it("it should return null because we didn't put anything in pageLimit at the beginning", function() {
  const pageLim = pagination.pageLimit;
  assert.equal(pageLim, null);
});

it("it should return null because we didn't put anything in totalRecords at the beginning", function() {
  const totalRecords = pagination.totalRecords;
  assert.equal(totalRecords, null);
});

it("left page", function() {
  const left_page = pagination.LEFT_PAGE;
  assert.equal(left_page, null);
});