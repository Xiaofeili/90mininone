import React, { Component } from 'react';
import "./Contact.css"

class Contact extends Component {
    render() {
        return (
            
            <div>
                <div className="space"></div>
                <div style={{ display: 'flex', justifyContent: 'center', padding: 30 }}>
                    <div><h2>Contact Page</h2><br></br>
                    </div>
                    
                </div>
                
                <div>
                    <center>
                    <p>You can contact us at</p>
                    <a href="mailto:90mininone@gmail.com">90Min In One Team</a>
                    </center>
                </div>
            </div>
        );
    }
}

export default Contact;