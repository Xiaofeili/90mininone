import React from 'react';
import {Card, Button} from "react-bootstrap";
import './HomeCard.css';
import { NavLink } from "react-router-dom"

const HomeCard = props => {
    return (
        <Card className="text-center homecard">
            <div className="overflow">
              <Card.Img className="homecard-img-top" variant="top" src={props.imgSrc} />
            </div>
            <Card.Body>
              <Card.Title>{props.imgTitle}</Card.Title>
              <Card.Text>{props.description}</Card.Text>
            </Card.Body>
            <Card.Body>
                <NavLink className="buttonlinktext" to="../Leagues">
                  <Button className="btn-x">{props.imgTitle}</Button>
                </NavLink>
            </Card.Body>
            <Card.Body>
            </Card.Body>
          </Card>
    );
}

export default HomeCard;