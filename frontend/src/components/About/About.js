import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import "./About.css";
import { Card, ListGroup, ListGroupItem, CardDeck, Container, Row, Col, Image, Button } from "react-bootstrap";
import mailIcon from "../../assets/media/mail.png";
import FlipCard from "../FlipCard/FlipCard";


function importAll(r) {
    let images = {};
    r.keys().map((item, index) => { images[item.replace('./', '')] = r(item); });
    return images;
}
const images = importAll(require.context('../../assets/media', false, /\.(png|jpe?g|svg)$/));


const Jinho = {
    role: "Front-End",
    expanded: "Test cases, sorting",
    description: "I'm Jinho"
};
const Shuyan = {
    role: "Front-End",
    expanded: "User Stories, UML diagrams, report",
    description: "I'm Shuyan"
};
const HoJae = {
    role: "Full-Stack",
    expanded: "AWS, TLS, Filter",
    description: "Fourth year CS major. I want a cat"
};
const Dominique = {
    role: "Back-End",
    expanded: "Database and API",
    description: "Third year CS major. I don't have a cat :("
};
const Andre = {
    role: "Front-End",
    expanded: "Aesthetics, Pagination, Search",
    description: "Third year CS major. I have a cat"
};

const Jiho = {
    role: "Full-Stack",
    expanded: "AWS, Database, Configuration, Tests",
    description: "Second year CS major. I used to have a cat"
};

var totalCommits = 0;
var totalIssues = 0;
var totalTests = 0;


class About extends Component {
    state = {
        members: [],
        issues: []
    }

    componentDidMount() {
        fetch('https://gitlab.com/api/v4/projects/16994542/repository/contributors',
            {
                headers: {
                    'Private-Token': 'zvxsdSex_ASBUDuVGuks',
                    'Content-Type': 'application/json'
                }
            }
        )
            .then(res => res.json())
            .then((data) => {
                this.setState({ members: data })
                console.log(this.state.members)
            })
            .catch(console.log)

        fetch('https://gitlab.com/api/v4/projects/16994542/issues?per_page=100',
            {
                headers: {
                    'Private-Token': 'zvxsdSex_ASBUDuVGuks',
                    'Content-Type': 'application/json'
                }
            }
        )
            .then(res => res.json())
            .then((data) => {
                this.setState({ issues: data })
                console.log(this.state.issues)
            })
            .catch(console.log)
    }

    abc = () => {
        totalCommits = 0;
        for (let i = 0; i < this.state.members.length; i++) {
            let member = this.state.members[i];
            if (member.email === "andre.tran22@yahoo.com" || member.email === "andretran@andres-mbp.home"
                || member.email === "andretran@Andres-MacBook-Pro.local" || member.email === "shuyan.li@utexas.edu"){
                    this.state.members.splice(i, 1);
                    i = 0;
            }
        }
        return this.state.members.map(member => {
            let counter = 0;
            let firstName = member.name.split(" ")[0].replace('-', '');
            console.log(member['name'] + ": " + member.email);
            totalCommits += member['commits'];


            this.state.issues.forEach(issue => {
                //console.log(issue);
                counter += issue['author']['name'] === member['name'] ? 1 : 0;
            });

            totalIssues += counter;
            // console.log("totalIssues: " + totalIssues);

            return (
                <Col lg={4} sm={6} style={{ display: 'flex', justifyContent: 'center' }}>
                    <p style={{ width: '85%' }}>
                        <Card className="text-center">
                            <Card.Img className="cardpic rounded" variant="top" src={images[(member.name + ".jpg")]} />
                            <Card.Body>
                                <Card.Title>{member['name']}</Card.Title>
                                <ListGroup className="list-group-flush">
                                    <ListGroupItem>
                                        <div>
                                            {eval(firstName).role}
                                        </div>
                                        <div>
                                            {eval(firstName).expanded}
                                        </div>
                                        </ListGroupItem>
                                    <ListGroupItem>
                                        <Row>
                                            <Col xs={4}><Image className="icon" src={images["icon-commit.png"]} fluid></Image></Col>
                                            <Col xs={4}><Image className="icon" src={images["icon-issue.png"]} fluid></Image></Col>
                                            <Col xs={4}><Image className="icon" src={images["icon-test.png"]} fluid></Image></Col>
                                        </Row>
                                        <Row>
                                            <Col>{member['commits']}</Col>
                                            <Col>{counter}</Col>
                                            <Col>{member['name'] === "Jiho Ro" ? 18 : member['name'] === "Jinho Yoon" ? 2 : member['name'] === "Dominique Tran" ? 13 : 0}</Col>
                                        </Row>
                                    </ListGroupItem>
                                </ListGroup>
                                <Card.Text className="mt-4">{eval(firstName).description}</Card.Text>
                            </Card.Body>
                        </Card>
                    </p>
                </Col>
            );
        }
        )
    };

    render() {
        return (
            // <React.Fragment>
            <Container className="text-center" style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                <Row className="space"></Row>
                <br></br>
                <br></br>
                <br></br>
                <Row className={"aboutMissionCard"}>
                    <Row className="text-center"><h1>ABOUT</h1></Row>
                    <br></br>
                    <Row className="about-row side">
                        <body >
                            Hey! We're the developers behind 90 min in one.
                            Get to know a little more about us below! Also, explore our API and discover the development tools that we used.
                        </body>
                    </Row>
                    <Row className="about-row side" style={{ marginTop: '2rem' }}><h3>OUR MISSION</h3></Row>
                    <Row className="about-row side" style={{ marginTop: '1rem' }}>
                        <body className="text-center">
                            Soccer players have one goal. Literally. Here at 90 min in one, we do too!
                            </body>
                    </Row>
                    <Row className="about-row side" style={{ marginTop: '1rem' }}>
                        <body >
                            With more than 4 billion fans worldwide, soccer has skyrocket to become the most popular sport.
                            Learning about soccer has never been easier with 90 min in one. We seek to provide an engaging site
                            that feeds the curiosity behind soccer statistics through the delivery of simple, informative data sets
                            about soccer leagues, teams, and players in a clean and understandable way.
                            </body>
                    </Row>
                    <Row className="about-row side" style={{ marginTop: '1rem' }}>
                        <body >
                            More precisely, we’ve gathered data on 1300+ leagues, 50 of the most popular teams, and their players.
                            Like a certain league? Find out what players have played under that league. Curious about a certain player?
                            Explore the team they play for. We’ve grouped data such that curiosity can flow seamlessly from category to
                            category and provide a quality learning experience.
                            </body>
                    </Row>
                    <Row className="about-row side" style={{ marginTop: '1rem' }}>
                        <body >
                            We hope you get a kick out of it! Get it?
                        </body>
                    </Row>
                    <br></br>
                    <br></br>
                    <Row className="text-center"><h3>VIDEO PRESENTATION</h3></Row>
                        <br></br>
                        <a href="https://youtu.be/56EMnyt-Emo">https://youtu.be/56EMnyt-Emo</a>
                    <Row></Row>
                </Row>
                

                <br></br>
                <br></br>
                <Row className={"aboutMissionCard"}>
                    <Row className="text-center"><h1>THE TEAM</h1></Row>
                    <br></br>
                    <br></br>
                    <Row className="about text-center">
                        {
                            this.abc()
                        }
                    </Row>
                </Row>
                <br></br>
                <br></br>
                <Row className={"aboutMissionCard"}>
                    <br></br>
                    <Row className="about-row"><h3>REPOSITORY STATISTICS</h3></Row>
                    <br></br>
                    <br></br>
                    <Row className="repostats" >
                        <Col xs={12} md={4}>
                            <p>
                                <Row className="about-row"><Image className="icon2" src={images["icon-commit.png"]} fluid></Image></Row>
                                <Row className="about-row" style={{ marginTop: '1rem' }}>Total Commits:</Row>
                                <Row className="about-row">{totalCommits}</Row>
                            </p>
                        </Col>
                        <Col xs={12} md={4}>
                            <p>
                                <Row className="about-row"><Image className="icon2" src={images["icon-issue.png"]} fluid></Image></Row>
                                <Row className="about-row" style={{ marginTop: '1rem' }}>Total Issues:</Row>
                                <Row className="about-row">{totalIssues}</Row>
                            </p>
                        </Col>
                        <Col xs={12} md={4}>
                            <p>
                                <Row className="about-row"><Image className="icon2" src={images["icon-test.png"]} fluid></Image></Row>
                                <Row className="about-row" style={{ marginTop: '1rem' }}>Total Tests:</Row>
                                <Row className="about-row">33</Row>
                            </p>
                        </Col>
                    </Row>
                </Row>
                <br></br>
                <br></br>
                <Row className={"aboutMissionCard"}>
                    <Row className="about-row"><h3>DEVELOPMENT TOOLS</h3></Row>
                    <br></br>
                    <br></br>
                    <Row className="about-row text-center">
                        <Col md={6} lg={3} className="d-flex justify-content-center align-items-center">
                            <p>
                                <div className="tool-icon">
                                    <FlipCard 
                                        image={images["react.png"]} 
                                        description="Javascript library for building our user interface"
                                        title="REACT"
                                        goTO="https://reactjs.org"
                                    ></FlipCard>
                                </div>
                            </p>
                        </Col>
                        <Col  md={6} lg={3} className="d-flex justify-content-center align-items-center"> 
                            <p>
                                <div className="tool-icon">
                                    <FlipCard 
                                        image={images["boot.png"]} 
                                        description="CSS framework for frontend components used on our site"
                                        title="BOOTSTRAP"
                                        goTo="https://getbootstrap.com"
                                    ></FlipCard>
                                </div>
                            </p>
                        </Col>
                        <Col md={6} lg={3} className="d-flex justify-content-center align-items-center">
                            <p>
                                <div className="tool-icon">
                                    <FlipCard 
                                        image={images["aws-logo.png"]} 
                                        description="Platform used to host our website"
                                        title="AWS"
                                        goTo="https://aws.amazon.com"
                                    ></FlipCard>
                                </div>
                            </p>
                        </Col>
                        <Col md={6} lg={3} className="d-flex justify-content-center align-items-center">
                            <p>
                                <div className="tool-icon">
                                    <FlipCard 
                                        image={images["bing.jpg"]} 
                                        description="Images API to fetch soccer images"
                                        title="BING IMAGES"
                                        goTo="https://azure.microsoft.com/en-us/services/cognitive-services/bing-image-search-api/"
                                    ></FlipCard>
                                </div>
                            </p>
                        </Col>
                        <Col md={6} lg={3} className="d-flex justify-content-center align-items-center"> 
                            <p>
                                <div className="tool-icon">
                                    <FlipCard 
                                        image={images["gitlab.png"]} 
                                        description="Version control to track issues and store our project repository"
                                        title="GITLAB"
                                        goTo="https://gitlab.com/jihoro/90mininone"
                                    ></FlipCard>
                                </div>
                            </p>
                        </Col>
                        <Col md={6} lg={3} className="d-flex justify-content-center align-items-center">
                            <p>
                                <div className="tool-icon"> 
                                    <FlipCard 
                                        image={images["API-fb.png"]} 
                                        description="Soccer API #1 to pull data"
                                        title="API FOOTBALL"
                                        goTo="https://www.api-football.com"
                                    ></FlipCard>
                                </div>
                            </p>
                        </Col>
                        <Col md={6} lg={3} className="d-flex justify-content-center align-items-center">
                            <p>
                                <div className="tool-icon"> 
                                    <FlipCard 
                                        image={images["fb-data.png"]} 
                                        description="Soccer API #2 to pull data"
                                        title="FOOTBALL-DATA.ORG"
                                        goTo="https://www.football-data.org"
                                    ></FlipCard>
                                </div>
                            </p>
                        </Col>
                        <Col md={6} lg={3} className="d-flex justify-content-center align-items-center">
                            <p>
                                <div className="tool-icon">
                                    <FlipCard 
                                        image={images["sportmonks.png"]} 
                                        description="Soccer API #3 to pull data"
                                        title="SPORT MONKS"
                                        goTo="https://www.sportmonks.com/"
                                    ></FlipCard>
                                </div>
                            </p>
                        </Col>
                    </Row>
                </Row>
                <br></br>
                <br></br>
                <Row className={"aboutMissionCard"}>
                    <Row >
                    <Col sm={6}> 
                        <Row className="about-row"><h3>OUR API</h3></Row>
                        <br></br>
                        <Row className="about-row">
                            <Button className="tool-icon" href="https://documenter.getpostman.com/view/10502654/SzS8uRNf">
                                <Image className="postman" src={images["postmanLogo.jpg"]} fluid></Image>
                            </Button>
                        </Row>
                    </Col>
                    <Col sm={6} >
                        <Row className="about-row"><h3>CONTACT US</h3></Row>
                        <Row className="about-row side" style={{ marginTop: '1rem' }}>
                            <p>
                                <Button className="tool-icon" href="mailto:90mininone@gmail.com">
                                    <Image className="postman" src={mailIcon} fluid></Image>
                                </Button>
                            </p>
                        </Row>
                    </Col>
                    </Row>
                </Row>
                <br></br>
                <br></br>
                <br></br>
            </Container>

        );
    }
}

export default About;