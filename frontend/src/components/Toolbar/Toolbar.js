import React, { Component } from 'react';
import {Navbar, Nav, NavbarBrand} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
import './Toolbar.css';

export default class Toolbar extends Component {
  render(){
      return(
          <Navbar className="toolbar" fixed="top"  expand="lg">

            {/* logo */}
            <NavbarBrand as={NavLink} to="/">90 min in one</NavbarBrand>

            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">

              {/* nav items */}
              <Nav className="ml-auto ">
                <Nav.Link as={NavLink} exact to="/">Home</Nav.Link>
                <Nav.Link as={NavLink} to="/Leagues">Leagues</Nav.Link>
                <Nav.Link as={NavLink} to="/Teams">Teams</Nav.Link>
                <Nav.Link as={NavLink} to="/Players">Players</Nav.Link>
                <Nav.Link as={NavLink} to ="/Search">Search</Nav.Link>
                <Nav.Link as={NavLink} to="/About">About</Nav.Link>
                <Nav.Link as={NavLink} to="/Visualizations">Visualizations</Nav.Link>
                <Nav.Link as={NavLink} to="/Provider">Provider</Nav.Link>
              </Nav>
            </Navbar.Collapse>
        </Navbar>
      )
  }
}
