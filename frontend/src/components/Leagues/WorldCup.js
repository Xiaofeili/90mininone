import React, { Component } from 'react';
import {Button} from 'react-bootstrap';
import jsonData from "../../assets/leagues.json";
import {Table} from 'reactstrap';
class WorldCup extends Component {
    state = {
        leagues: {}
    }
    componentDidMount() {
            this.setState({leagues:jsonData.api.leagues[0]}, 
                () => {console.log(this.state.leagues);}) 
    }    
    abc = () => (
                
        <tr>
            <td>{this.state.leagues['name']}</td>
            <td>{this.state.leagues['country']}</td>
            <td>{this.state.leagues['country_code']}</td>
            <td><img src = {this.state.leagues['logo']} width="30"></img></td>
            <td><img src = {this.state.leagues['flag']} width="30"></img></td>
        </tr>
    );

    

    get_tables = () => {

        let attrs =  Object.keys(this.state.leagues).filter(
            key => ['name', 'type', 'country', 'flag', 'country_code', 'logo', 'league_id', 'coverage'].indexOf(key) == -1
        );
        
        return (
            <Table>
                <thead>
                    <tr>
                        {attrs.map(attr => <th>{attr}</th>)}
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        {attrs.map(attr => <td>{this.state.leagues[attr]}</td>)}
                    </tr>
                </tbody>
            </Table>
        );
    };

    render() {
        return (
            <React.Fragment>
                <div style={{ display: 'flex', justifyContent: 'center', padding: 30 }}>
                    <div><h2>World Cup</h2></div>
                </div>
                <div>
                    <Table>
                        <thead>
                            <tr>
                                <th>League Name</th>
                                <th>Country</th>
                                <th>Country Code</th>
                                <th>Logo</th>
                                <th>Flag</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.abc()}
                        </tbody>
                    </Table>
                    {this.get_tables()}
                </div>
            </React.Fragment>
        );
    }
}

export default WorldCup;