import React from "react";
import Highlight from "../Highlight/Highlight";
import { Col, Card, ListGroup, ListGroupItem, Button, Image } from 'react-bootstrap';
import { Link } from "react-router-dom"

export default (props) => {

    const { name = {}, country = {}, founded = {}, venue_name = {}, logo = {} } = props.team;
    const searchQuery = props.searchQuery;

    console.log(name);
    console.log(searchQuery);

    const nameHighlight = <Highlight searchQuery = {searchQuery} word = {name.toString()} />
    const foundedHighlight = <Highlight searchQuery = {searchQuery} word = {founded.toString()} />
    const countryHighlight = <Highlight searchQuery = {searchQuery} word = {country} />
    const venueHighlight = <Highlight searchQuery = {searchQuery} word = {venue_name} />

    return (
        <Col lg={3} md={6} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Card className="team-card">

                {/* Team Logo */}
                <div className="image-card">
                    <Image className="team-logo" src={logo} rounded />
                </div>

                <Card.Body className="cb">
                    <Card.Title>{nameHighlight}</Card.Title>
                    <ListGroup className="list-group-flush">

                        {/* Founded */}
                        <ListGroupItem>
                            <div className="subtitle" style={{ display: 'inline-block' }}>
                                Founded: &#160;
                            </div>
                            <div style={{ display: 'inline-block' }}>{foundedHighlight}</div>
                        </ListGroupItem>

                        {/* Country */}
                        <ListGroupItem>
                            <div className="subtitle" style={{ display: 'inline-block' }}>Country: &#160;</div>
                            <div style={{ display: 'inline-block' }}>{countryHighlight}</div>
                        </ListGroupItem>

                        {/* Venue Name*/}
                        <ListGroupItem>
                            <div className="subtitle" style={{ display: 'inline-block' }}>
                                Venue Name: &#160;
                                    </div>
                            <div style={{ display: 'inline-block' }}>{venueHighlight}</div>
                        </ListGroupItem>

                        {/* More Info button */}
                        <ListGroupItem>
                            <Link className="linktext" to={{
                                pathname: '/Teams/' + name.replace(' ', ''),
                                state: {
                                    team_id: props.team.team_id
                                }
                            }}>
                                <Button>More Info</Button>
                            </Link>
                        </ListGroupItem>
                    </ListGroup>
                </Card.Body>
            </Card>
        </Col>
    );

}

