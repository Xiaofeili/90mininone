import React, { useState, useEffect } from 'react';
import { Container, Row, Col, ListGroup, Image, Accordion, Card, Button } from 'react-bootstrap';
import baseUrl from '../../assets/baseUrl';
import InfiniteScroll from "react-infinite-scroll-component";
import { Link } from "react-router-dom";
import "./Teams.css"


const TeamInstancePage = (props) => {
    const team_id = props.location.state.team_id;
    console.log(team_id)

    const [team, setTeam] = useState({});
    const [leagues, setLeagues] = useState(null);

    async function fetchTeam() {
        const res = await fetch(baseUrl + "/Teams/" + team_id);
        res
            .json()
            .then(res => setTeam(res));
    }

    useEffect(() => {
        fetchTeam();
    }, []);

     const {
        logo,
        name,
        founded,
        country,
        code,
        venue_name,
        venue_city,
        venue_surface,
        venue_address,
        is_national,
        players
    } = team;

    //fetches league based on team_id and sets state "league"
    async function fetchLeagues(tId) {
        console.log(tId);
        const res = await fetch(baseUrl + "/TeamAndLeague?team_id=" + tId);
        res
            .json()
            .then(res => setLeagues(res));
    }

    //use effect that gets called when team_id changes from 0 to x. Calls fetchLeagues()
    useEffect(() => {
        fetchLeagues(team_id);
        console.log(players)
    }, [team_id]);

    return (
        <div className="background">
        <Container className="instanceCon">
            <Row className="space"></Row>
            <br></br>
            <br></br>
            <br></br>
            <Row style={{display:'flex', flexWrap:'nowrap'}}>
                <Col className="teamStatsCard" sm={4}>
                    <Row>
                        <div className="d-flex justify-content-center align-items-center text-center">
                            <h1 className="text-center">{name}</h1>
                        </div>
                    </Row>
                    <br></br>
                    <Row>
                        <ListGroup variant="flush">
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Founded: &#160;</div>
                                <div >{founded}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Country: &#160;</div>
                                <div >{country}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Code: &#160;</div>
                                <div >{code}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Venue Name: &#160;</div>
                                <div >{venue_name}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Venue City: &#160;</div>
                                <div >{venue_city}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Venue Surface: &#160;</div>
                                <div >{venue_surface}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">Venue Address: &#160;</div>
                                <div >{venue_address}</div>
                            </ListGroup.Item>
                            <ListGroup.Item className="d-flex flex-row">
                                <div className="subtitle">National Team: &#160;</div>
                                <div >{is_national ? 'Yes' : 'No'}</div>
                            </ListGroup.Item>
                        </ListGroup>
                    </Row>
                </Col>

                <Col className="teamAndLeagueCards" sm={4}>
                    <Row className="teamCardLink">
                        <Image className="teamLogoImage" src={logo} rounded fluid />
                    </Row>
                    <br></br>
                    <br></br>
                    <Row>
                        <div className="leagueCardLink flex-column ">
                            <h3>LEAGUES</h3>
                            <br></br>
                            <div>
                                {leagues != null && leagues.length != 0 ? (
                                    <div>
                                        {leagues
                                            .filter(x => {return x.league != 'None'})
                                            .map(league => (
                                                <Link className="linktext" to={{
                                                    pathname: '/Leagues/' + league.league.replace(' ', ''),
                                                    state: {
                                                        league_id: league.league_id
                                                    }
                                                }}>
                                                    <h3>{league.league}</h3>
                                                </Link>
                                            ))
                                        }
                                    </div>
                                ) : 'no related league data to display'}
                            </div>
                        </div>
                    </Row>
                </Col>

                <Col sm={4}>
                    <div className="playersCardLink flex-column" >
                        <h3>PLAYERS</h3>
                        <br></br>
                        <InfiniteScroll
                            dataLength={players != null && players.length != 0 ? players.length : 0}
                            style={{display:'inline-flex', maxHeight: '400px', overflow: 'auto', height:'auto'}}
                            height={400}
                            >
                            {players != null && players.length != 0 ? (
                                    <div>
                                        {players
                                            .map(player => (
                                                <Link className="linktext" to={{
                                                    pathname: '/Players/' + player.player_name.replace(' ', ''),
                                                    state: {
                                                        player_id: player.player_id
                                                    }
                                                }}>
                                                    <p>{player.player_name}</p>
                                                </Link>
                                            ))
                                        }
                                    </div>
                                ) : 'no related player data to display'}
                        </InfiniteScroll>
                    </div>
                </Col>
            </Row>
        </Container>
        </div>
    );
}

export default TeamInstancePage;