import React from "react";
import FadeIn from "react-fade-in";
import Lottie from "react-lottie";
import * as redCheck from "./redCheck.json";
import * as juggle from "./juggle.json";



const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: juggle.default,
    rendererSettings: {
        preserveAspectRatio: "xMidYMid slice"
    }
};

const defaultOptions2 = {
    loop: false,
    autoplay: true,
    animationData: redCheck.default,
    rendererSettings: {
       preserveAspectRatio: "xMidYMid slice"
    }
 };
const Loading = ({loading}) => {
    return (
        <FadeIn>
            <div className="d-flex flex-column justify-content-center align-items-center" style={{height:'50vh'}} >
                <div >
                    <h3>fetching data...</h3>
                </div> 
               <div>
                {!loading ? (
                    <Lottie options={defaultOptions} height={150} width={150} />
                ) : (
                    <Lottie options={defaultOptions2} height={150} width={150} />
                )}
                </div>
                
            </div>
        </FadeIn>
    )
}

export default Loading;