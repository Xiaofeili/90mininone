import React from "react";
import { Container, Row, Col, Image, CardDeck, Card } from 'react-bootstrap';
import { Spring } from 'react-spring/renderprops';
import { useSpring, animated } from 'react-spring';
import { Link, animateScroll as scroll } from "react-scroll";
import { NavLink } from "react-router-dom"

import "./Home.css";
import HomeCard from "../HomeCard/HomeCard";

function importAll(r) {
    let images = {};
    r.keys().map((item, index) => { images[item.replace('./', '')] = r(item); });
    return images;
}
const images = importAll(require.context('../../assets/media', false, /\.(png|jpe?g|svg)$/));


const Home = () => {

    const animateFade = useSpring({
        from: { opacity: 0 },
        to: async next => {
            while (1) {
                await next({ opacity: 0 })
                await next({ opacity: 1 })
            }
        },
    });

    return (
        <Container>

            {/* Text and Picture */}
            <Row className="outer-row overall">
                <Col className="overall" sm={6}>
                    <div>
                        <h1>Welcome!</h1>
                        <h2>Learn more about soccer with us, your go-to soccer experts. We'll tell you all you need to know about your favorite teams, leagues, and players!</h2>
                        <div className="btn-get-started">
                            <Link
                                activeClass="active"
                                to="cardButtons"
                                spy={true}
                                smooth={true}
                                offset={-80}
                                duration={800}
                                className="buttonContent"
                                >
                                Get Started
                                <animated.div style={animateFade}>
                                    <Image src={images['belowArrow.png']} className="arrowPic" />
                                </animated.div>
                            </Link>


                        </div>

                    </div>
                </Col>
                <Col sm={6}>
                    <Image
                        src={images['soccer_boy.png']}
                        className="animated boy"
                        alt="Soccer Boy Image"
                        fluid />
                </Col>
            </Row>
        
            <Row section id="cardButtons" className="buttonCon">
                {/* <Row><div className="spacerHome"></div></Row> */}
                <CardDeck>
                    <HomeCard
                        imgSrc={images['soccer-league-logos.jpg']}
                        imgTitle="Leagues"
                        description="Ever wonder who the top teams and players from each league are? Well wonder no more, find out here! "
                        goTo="../Leagues" />
                    <HomeCard
                        imgSrc={images['soccer-teams.jpg']}
                        imgTitle="Teams"
                        description="With 200+ teams, there's much to learn! Find out stats such as what country a team is from, their standing in a league, their players, and more! "
                        goTo="../Teams" />
                    <HomeCard
                        imgSrc={images['soccer-player.jpg']}
                        imgTitle="Players"
                        description="From Lionel Messi to Cristiano Ronaldo, we've got who you're looking for. Find out more about your favorite players! "
                        goTo="../Players" />
                </CardDeck>
            </Row>

            
            {/* SPACER */}
            {/* <Row><div className="spacer"></div></Row> */}

        </Container>

    );
}

export default Home;
