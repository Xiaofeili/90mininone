import React, { Component } from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

//nav bar things
import Toolbar from './Toolbar/Toolbar';

//routes to all pages
import Routes from './Routes';

class App extends Component{
  render() {
    return (
      <div style={{height: '100%'}}>
          <Routes />
      </div>
    );
  }
}

export default App;
