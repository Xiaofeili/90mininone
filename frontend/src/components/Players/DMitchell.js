import React, { Component } from 'react';
//import { Card, ListGroup } from 'react-bootstrap';
import jsonData from "../../assets/players.json";
import {Table} from 'reactstrap';
class DMitchell extends Component {
    state = {
        player: {}
    }
    componentDidMount() {
            this.setState({player:jsonData.api.players[0]}, 
                () => {console.log(this.state.player);}) 
    }    

    renderTableContents = () => Object.keys(this.state.player).filter(
                key => ['player_id', 'player_name', 'logo', 'team_id', 'is_national'].indexOf(key) == -1
            ).map(key => (
                <tr>
                    <center>
                    <th class>{key}</th>
                    <td>{this.state.player[key]}</td>
                    </center>
                </tr>
            ));

    render() {
        return (
            <React.Fragment>
            <div style={{ display: 'flex', justifyContent: 'center', padding: 30 }}>
                <div><h2>D. Mitchell</h2></div>
                <br></br>
                
            </div>
            <center><img src = "https://www.manutd.com/AssetPicker/images/0/0/10/246/718495/CD2I223681534782651164_thumb.jpg" width ="80"></img></center>
            
            <div>
                <Table>
                    <tbody>
                        {this.renderTableContents()}
                    </tbody>
                </Table>
                </div>
            </React.Fragment>
        );
    }
}

export default DMitchell;