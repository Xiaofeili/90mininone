import React from "react";
import Highlight from "../Highlight/Highlight";
import { Col, Card, ListGroup, ListGroupItem, Button, Image } from 'react-bootstrap';
import { Link } from "react-router-dom"


const fetchData = (firstname, lastname) => {
    const first = firstname;
    const last = lastname;
    //do fetch call to get headshot
}

export default (props) => {
    const { player_name = {}, position = {} , age = {}, league = {}, team = {}, firstname = {}, lastname = {}, nationality = {} } = props.player;
    const searchQuery = props.searchQuery;

    console.log(player_name);
    console.log(searchQuery);

    const name = firstname + " " + lastname;
    const nameHighlight = <Highlight searchQuery = {searchQuery} word = {name} />
    const teamNameHighlight = <Highlight searchQuery = {searchQuery} word = {team.name} />
    const positionHighlight = <Highlight searchQuery = {searchQuery} word = {position} />
    const leagueHighlight = <Highlight searchQuery = {searchQuery} word = {league} />
    const ageHighlight = <Highlight searchQuery = {searchQuery} word = {age.toString()} />
    const nationalityHighlight = <Highlight searchQuery = {searchQuery} word = {nationality} />

    const imageSrc = fetchData(firstname, lastname);

    return (
        <Col lg={3} md={6} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Card className="player-card">

                {/* Player Image */}
                {/* <div className="image-card">
                    <Image className="league-logo" src={null} rounded />
                </div> */}
                <br></br>

                <Card.Body className="cb">
                    <Card.Title>{nameHighlight}</Card.Title>
                    <ListGroup className="list-group-flush">

                        {/* Team */}
                        <ListGroupItem>
                            <div className="subtitle" style={{ display: 'inline-block' }}>Team: &#160; </div>
                            <div style={{ display: 'inline-block' }}>{teamNameHighlight}</div>
                            <div><img src={team.logo} width="30"></img></div>
                        </ListGroupItem>

                        {/* Position */}
                        <ListGroupItem>
                            <div className="subtitle" style={{ display: 'inline-block' }}>Position: &#160; </div>
                            <div style={{ display: 'inline-block' }}>{positionHighlight}</div>
                        </ListGroupItem>

                        {/* League */}
                        <ListGroupItem>
                            <div className="subtitle" style={{ display: 'inline-block' }}> League: &#160; </div>
                            <div style={{ display: 'inline-block' }}>{leagueHighlight}</div>
                        </ListGroupItem>

                        {/* Age */}
                        <ListGroupItem>
                            <div className="subtitle" style={{ display: 'inline-block' }}> Age: &#160; </div>
                            <div style={{ display: 'inline-block' }}>{ageHighlight}</div>
                        </ListGroupItem>

                        {/* Nationality */}
                        <ListGroupItem>
                            <div className="subtitle" style={{ display: 'inline-block' }}> Nationality: &#160; </div>
                            <div style={{ display: 'inline-block' }}>{nationalityHighlight}</div>
                        </ListGroupItem>

                        {/* More Info button */}
                        <ListGroupItem>
                            <Link className="linktext" to={{
                                pathname: '/Players/' + player_name.replace(' ', ''),
                                state: {
                                    player_id: props.player.player_id
                                }
                            }}>
                                <Button>More Info</Button>
                            </Link>
                        </ListGroupItem>

                    </ListGroup>
                </Card.Body>
            </Card>
        </Col>
    );

}


