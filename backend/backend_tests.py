from unittest import main, TestCase
from app import app


class MyUnitTests (TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

        response = self.app.get("/Players")
        self.players = response.get_json()
        response = self.app.get("/Teams")
        self.teams = response.get_json()
        response = self.app.get("/Leagues")
        self.leagues = response.get_json()

    def test_1(self):
        response = self.app.get('/Players')
        self.assertEqual(response.status_code, 200)

    def test_2(self):
        response = self.app.get('/Teams')
        self.assertEqual(response.status_code, 200)

    def test_3(self):
        response = self.app.get('/Leagues')
        self.assertEqual(response.status_code, 200)

    def test_4(self):
        result = True
        player = self.players["players"][1]
        result &= (
            "player_id" in player
            and "player_name" in player
            and "firstname" in player
            and "lastname" in player
            and "position" in player
            and "age" in player
            and "league" in player
            and "team" in player
        )
        self.assertTrue(result)

    def test_5(self):
        result = True
        team = self.teams["teams"][1]
        result &= (
            "team_id" in team
            and "name" in team
            and "founded" in team
            and "country" in team
            and "logo" in team
            and "venue_name" in team
        )
        self.assertTrue(result)

    def test_6(self):
        result = True
        league = self.leagues["leagues"][1]
        result &= (
            "league_id" in league
            and "name" in league
            and "country" in league
            and "country_code" in league
            and "flag" in league
            and "logo" in league
        )
        self.assertTrue(result)

    def test_7(self):
        self.assertEqual(len(self.leagues["leagues"]), 126)

    # def test_8(self):
    #     self.assertEqual(len(self.teams["teams"]), 651)

    # def test_9(self):
    #     self.assertEqual(len(self.players["players"]), 3146)

    def test_10(self):
        response = self.app.get("/Leagues/254")
        league = response.get_json()
        rl = {
            "country": "Luxembourg",
            "country_code": "LU",
            "flag": "https://media.api-sports.io/flags/lu.svg",
            "league_id": "254",
            "logo": "https://media.api-sports.io/leagues/254.jpg",
            "name": "National Division",
            "season": 2018,
            "season_end": "2019-05-19",
            "season_start": "2018-08-05",
            "type": "League"
        }
        for key in rl:
            if(key != "standings"):
                assert rl[key] == league[key]

    def test_11(self):
        response = self.app.get("/Players/119")
        player = response.get_json()
        rl = {
            "age": 34, 
            "birth_date": "10/02/1986",
            "birth_place": "Santa Marta",
            "firstname": "Radamel Falcao",
            "height": "177 cm",
            "lastname": "Garc\u00eda Z\u00e1rate",
            "league": "Premier League",
            "nationality": "Colombia",
            "player_id": 119,
            "player_name": "R. Falcao",
            "position": "Attacker",
            "team": {
                "logo": "https://media.api-sports.io/teams/33.png",
                "name": "Manchester United",
                "team_id": 33
            },
            "weight": "72 kg"
        }
        for key in rl:
            assert rl[key] == player[key]

    def test_12(self):
        response = self.app.get('/Teams/random/random')
        self.assertEqual(response.status_code, 404)


if __name__ == "__main__":  # pragma: no cover
    main()
