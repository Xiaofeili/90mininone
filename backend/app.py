from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow import fields
from flask_cors import CORS, cross_origin

app = Flask(__name__)
app.config.from_pyfile("config.py")
CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

db = SQLAlchemy(app)
ma = Marshmallow(app)

application = app


########## PYTHON REPRESENTATIONS OF DB MODELS ##########

# Player Model
class Player(db.Model):

    player_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    player_name = db.Column(db.Integer)
    firstname = db.Column(db.String)
    lastname = db.Column(db.String)
    position = db.Column(db.String)
    age = db.Column(db.Integer)
    birth_date = db.Column(db.String)
    birth_place = db.Column(db.String)
    birth_country = db.Column(db.String)
    nationality = db.Column(db.String)
    height = db.Column(db.String)
    weight = db.Column(db.String)
    league = db.Column(db.String)
    team_id = db.Column(db.Integer, db.ForeignKey('team.team_id'),
                        nullable=False)


# Team Model
class Team(db.Model):

    team_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String)
    code = db.Column(db.String)
    logo = db.Column(db.String)
    country = db.Column(db.String)
    is_national = db.Column(db.Boolean)
    founded = db.Column(db.Integer)
    venue_name = db.Column(db.String)
    venue_surface = db.Column(db.String)
    venue_address = db.Column(db.String)
    venue_city = db.Column(db.String)
    venue_capacity = db.Column(db.Integer)
    players = db.relationship('Player', backref='team', lazy=True)


# League Model
class League(db.Model):

    league_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String)
    type = db.Column(db.String)
    country = db.Column(db.String)
    country_code = db.Column(db.String)
    season = db.Column(db.Integer)
    season_start = db.Column(db.String)
    season_end = db.Column(db.String)
    logo = db.Column(db.String)
    flag = db.Column(db.String)
    standings = db.Column(db.Boolean)


class TeamAndLeague(db.Model):
    league = db.Column(db.String)
    league_id = db.Column(db.Integer, primary_key=True)
    team = db.Column(db.String)
    team_id = db.Column(db.Integer, primary_key=True)


########## SCHEMAS ##########

# Player Schema
class PlayerSchema(ma.Schema):
    player_id = fields.Int(required=True)
    player_name = fields.Str(required=False)
    firstname = fields.Str(required=False)
    lastname = fields.Str(required=False)
    position = fields.Str(required=False)
    age = fields.Int(required=False)
    league = fields.Str(required=False)
    team = fields.Nested(lambda: TeamSchema(only=('name', 'logo', 'team_id')))
    nationality = fields.Str(required=False)


# Player Instance Schema with more attributes
class PlayerInstanceSchema(PlayerSchema):
    birth_date = fields.Str(required=False)
    birth_place = fields.Str(required=False)
    height = fields.Str(required=False)
    weight = fields.Str(required=False)


# Team Schema
class TeamSchema(ma.Schema):
    team_id = fields.Int(required=True)
    name = fields.Str(required=False)
    founded = fields.Int(required=False)
    country = fields.Str(required=False)
    logo = fields.URL(required=False)
    venue_name = fields.Str(required=False)


# Team Instance Schema with more attributes
class TeamInstanceSchema(TeamSchema):
    is_national = fields.Bool(required=False)
    venue_city = fields.Str(required=False)
    venue_address = fields.Str(required=False)
    venue_surface = fields.Str(required=False)
    code = fields.Str(required=False)
    players = fields.List(fields.Nested(PlayerSchema(only=('player_name', 'position', 'player_id'))))


# League Schema
class LeagueSchema(ma.Schema):
    league_id = fields.Str(required=True)
    name = fields.Str(required=False)
    country = fields.Str(required=False)
    country_code = fields.Str(required=False)
    flag = fields.URL(required=False)
    logo = fields.URL(required=False)
    type = fields.URL(required=False)


# League Instance Schema with more attributes
class LeagueInstanceSchema(LeagueSchema):
    season = fields.Int(required=False)
    season_start = fields.Str(required=False)
    season_end = fields.Str(required=False)
    standings = fields.Bool(required=False)


class TeamAndLeagueSchema(ma.Schema):
    league = fields.Str(required=True)
    league_id = fields.Int(required=True)
    team = fields.Str(required=True)
    team_id = fields.Int(required=True)


##### INITIALIZING SCHEMA OBJECTS #####

player_schema = PlayerInstanceSchema()
players_schema = PlayerSchema(many=True)

team_schema = TeamInstanceSchema()
teams_schema = TeamSchema(many=True)

league_schema = LeagueInstanceSchema()
leagues_schema = LeagueSchema(many=True)

team_and_league_schema = TeamAndLeagueSchema(many=True)


########## ENDPOINTS ##########

# Retrieve all players
@app.route('/Players', methods=['GET'])
@cross_origin()
def get_players():

    nationality = request.args.get('nationality', default='')
    position = request.args.get('position', default='')

    if (nationality != '' and position != ''):
        players = Player.query.filter_by(nationality=nationality, position=position).order_by(Player.player_id)
    elif (nationality != ''):
        players = Player.query.filter_by(nationality=nationality).order_by(Player.player_id)
    elif (position != ''):
        players = Player.query.filter_by(position=position).order_by(Player.player_id)
    else:
        players = Player.query.order_by(Player.player_id).all()

    result = players_schema.dump(players)
    return jsonify({'players': result})

# Retrieve single player entry by id
@app.route('/Players/<id>', methods=['GET'])
@cross_origin()
def get_player(id):
    player = Player.query.get(id)
    return player_schema.jsonify(player)

# Retrieve all teams
@app.route('/Teams', methods=['GET'])
@cross_origin()
def get_teams():

    country = request.args.get('country', default='')

    if (country != ''):
        teams = Team.query.filter_by(country=country).order_by(Team.team_id)
    else:
        teams = Team.query.order_by(Team.team_id).all()

    result = teams_schema.dump(teams)
    return jsonify({'teams': result})

# Retrieve single team by id
@app.route('/Teams/<id>', methods=['GET'])
@cross_origin()
def get_team(id):
    team = Team.query.get(id)
    return team_schema.jsonify(team)

# Retrieve all leagues
@app.route('/Leagues', methods=['GET'])
@cross_origin()
def get_leagues():

    type = request.args.get('type', default='')
    country = request.args.get('country', default='')

    if (type != '' and country != ''):
        leagues = League.query.filter_by(type=type, country=country).order_by(League.league_id)
    elif (type != ''):
        leagues = League.query.filter_by(type=type).order_by(League.league_id)
    elif (country != ''):
        leagues = League.query.filter_by(country=country).order_by(League.league_id)
    else:
        leagues = League.query.order_by(League.league_id).all()

    result = leagues_schema.dump(leagues)
    return jsonify({'leagues': result})

# Retrieve single league by id
@app.route('/Leagues/<id>', methods=['GET'])
@cross_origin()
def get_league(id):
    league = League.query.get(id)
    return league_schema.jsonify(league)

# Retrieve list of leagues from team id
@app.route('/TeamAndLeague')
@cross_origin()
def get_leagues_or_teams():
    id = request.args.get('team_id', default='')
    name = request.args.get('league', default='')

    if (id != ''):
        teamAndLeague = TeamAndLeague.query.filter_by(team_id=id).with_entities(TeamAndLeague.league, TeamAndLeague.league_id)
    elif(name != ''):
        teamAndLeague = TeamAndLeague.query.distinct(TeamAndLeague.team_id).filter_by(league=name).with_entities(TeamAndLeague.team, TeamAndLeague.team_id)

    return team_and_league_schema.jsonify(teamAndLeague)


if __name__ == "__main__":
    app.run()
